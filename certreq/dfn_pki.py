from zeep import Client, Settings, Plugin, xsd
from zeep.transports import Transport
import requests


# source: https://gitlab.physik.lmu.de/mitterer/dfn-pki/-/blob/master/src/DFN_PKI.py
# *******************************************************************************
# *** DFN PKI Interface                                                       ***
# *******************************************************************************
class ExplicitAltNamesTypes(Plugin):
    def egress(self, envelope, http_headers, operation, binding_options):
        if operation.name == "newRequest":
            ns = envelope.nsmap
            altnames = envelope.xpath("//AltNames")[0]
            altnames.set(f"{{{ns['soap-enc']}}}arrayType", "xsd:string[]")
            altnames.set(f"{{{ns['xsi']}}}type", "soap-enc:Array")
            for an in altnames.iterchildren():
                an.set(f"{{{ns['xsi']}}}type", "xsd:string")

        return envelope, http_headers


class DFNPKI:
    def __init__(self, ca, ra_operator_credentials_pathname=None):
        if ra_operator_credentials_pathname is None:
            soap_client_wsdl = f"https://pki.pca.dfn.de/{ca}/cgi-bin/pub/soap?wsdl=1"
            soap_client_transport = None
        else:
            # read RA operator credentials (that is: key and certificate)
            with open(ra_operator_credentials_pathname, "rb") as f:
                self.ra_operator_credentials = f.read()

            soap_client_wsdl = f"https://ra.pca.dfn.de/{ca}/cgi-bin/ra/soap?wsdl=1"
            soap_client_session = requests.Session()
            soap_client_session.cert = ra_operator_credentials_pathname
            soap_client_transport = Transport(session=soap_client_session)

        soap_client_settings = Settings(extra_http_headers={"Content-Type": "text/xml;charset=UTF-8"})
        self.soap_client = Client(soap_client_wsdl, transport=soap_client_transport, settings=soap_client_settings,
                                  plugins=[ExplicitAltNamesTypes()])

        self.soap_client.set_ns_prefix("xsd", "https://www.w3.org/2001/XMLSchema")
        self.soap_client.set_ns_prefix("xsi", "https://www.w3.org/2001/XMLSchema-instance")
        self.soap_client.set_ns_prefix("soap-enc", "https://schemas.xmlsoap.org/soap/encoding/")
        self.soap_client.set_ns_prefix("soap-env", "https://schemas.xmlsoap.org/soap/envelope/")
        self.soap_client.set_ns_prefix("dfncert-pub", "https://pki.pca.dfn.de/DFNCERT/Public")

    def submit_request(self, ra, requester_name, requester_email, requester_ou, csr, subj_alt_names, role,
                       revocation_passphrase, publish=False):

        return self.soap_client.service.newRequest(RaID=ra,
                                                   PKCS10=csr,
                                                   AltNames=subj_alt_names,
                                                   Role=role,
                                                   Pin=revocation_passphrase,
                                                   AddName=requester_name,
                                                   AddEMail=requester_email,
                                                   AddOrgUnit=requester_ou,
                                                   Publish=publish,
                                                   Subject=xsd.SkipValue)

    def retrieve_request_form(self, ra, request_number, revocation_passphrase):

        return self.soap_client.service.getRequestPrintout(RaID=ra,
                                                           Serial=request_number,
                                                           Format="application/pdf",
                                                           Pin=revocation_passphrase)

    def retrieve_certificate(self, ra, request_number, revocation_passphrase):

        return self.soap_client.service.getCertificateByRequestSerial(RaID=ra,
                                                                      Serial=request_number,
                                                                      Pin=revocation_passphrase)

    def retrieve_request(self, request_number):
        request = self.soap_client.service.getRawRequest(Serial=request_number)

        # check whether the request contains exactly one header and one CSR
        if request.count(b"-----BEGIN HEADER-----") != 1 or request.count(
                b"-----END HEADER-----") != 1 or request.count(
                b"-----BEGIN CERTIFICATE REQUEST-----") != 1 or request.count(
                b"-----END CERTIFICATE REQUEST-----") != 1:
            raise ValueError(
                "Error: The retrieved remote request contains not exactly one header block and exactly one CSR block.")

        # split up the request in header and CSR
        # The following assumes a response format like:
        # -----BEGIN HEADER-----\n
        # …\n
        # -----END HEADER-----\r\n
        # -----BEGIN CERTIFICATE REQUEST-----\n
        # …\n
        # -----END CERTIFICATE REQUEST-----
        request = request.partition(b"\n-----END HEADER-----\r\n")

        request_header = request[0] + request[1].rstrip(b"\r\n")
        request_csr = request[2]

        return (request_header, request_csr)

    def approve_request(self, request_number, csr):
        from cryptography import x509
        from cryptography.hazmat.primitives import hashes, serialization
        from cryptography.hazmat.primitives.serialization import pkcs7

        ra_operator_key = serialization.load_pem_private_key(self.ra_operator_credentials, None)
        ra_operator_certificate = x509.load_pem_x509_certificate(self.ra_operator_credentials)
        signature = pkcs7.PKCS7SignatureBuilder().set_data(csr).add_signer(ra_operator_certificate, ra_operator_key,
                                                                           hashes.SHA256()).sign(
            serialization.Encoding.PEM, [pkcs7.PKCS7Options.DetachedSignature, pkcs7.PKCS7Options.Binary])

        return self.soap_client.service.approveRequest(Serial=request_number,
                                                       Content=csr,
                                                       Signature=signature)

# Copyright © 2022 Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>
# Copyright © 2022 Xavier Mol <xavier.mol@kit.edu>
#
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
