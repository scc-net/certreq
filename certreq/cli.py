#!/usr/bin/env python3

import argparse
import hashlib
import ipaddress
import os
import sys
from pathlib import Path
from typing import List

from cryptography import x509
from cryptography.hazmat.primitives.serialization import Encoding
from zeep.exceptions import Error

from .dfn_pki import DFNPKI

# https://www.ca.kit.edu/p/zertifikatsprofile
certificate_role_choices = [
    '802.1X Client',
    'Domain Controller',
    'Exchange Server',
    'LDAP Server',
    'Mail Server',
    'Radius Server',
    'Shibboleth IdP SP',
    'VoIP Server',
    'VPN Server',
    'Web Server',
    'Web Server Must Staple',
    '802.1X User',
    'Code Signing',
    'Mitarbeiter',
    'RA Operator',
    'Smartcard',
    'Smartcard Encrypt',
    'Smartcard Sign',
    'Smartcard Sign and Logon',
    'Student',
    'User',
    'UserAuth',
    'UserEMail',
    'UserEncrypt',
    'UserSign',
    'UserSignAuth',
    'VPN User'
]

DEFAULT_RA_ID = 0
DEFAULT_CA = "kit-ca-g2"


def get_args():
    """Parse command line arguments and decide which function to call"""
    parser = argparse.ArgumentParser(description='Request certificates from  KIT-CA via API')
    parser.add_argument('--debug', dest='debug', action='store_true', default=False,
                        help='Print debug messages')
    parser.add_argument('--ca', dest='ca', default=DEFAULT_CA,
                        help=f'CA (default: {DEFAULT_CA})')
    parser.add_argument('--raid', dest='ra_id', default=DEFAULT_RA_ID,
                        help=f'RA Number (default: {DEFAULT_RA_ID})')

    subparsers = parser.add_subparsers()

    parser_new_request = subparsers.add_parser('newrequest', help='Upload CSR to the CA')
    parser_new_request.add_argument('--san', dest='san', nargs='*',
                                    help='Provide SANs including prefix yourself.')
    parser_new_request.add_argument('--dnssan', dest='dnssan', nargs='*',
                                    help='List of DNS subject alternative names (default: from CSR)')
    parser_new_request.add_argument('--ipsan', dest='ipsan', nargs='*',
                                    help='List of IP subject alternative names (default: from CSR)')
    parser_new_request.add_argument('--role', dest='role', default='Web Server', choices=certificate_role_choices,
                                    help='Certificate role (default: Web Server)')
    parser_new_request.add_argument('--name', dest='name', required=True,
                                    help='Requestor name')
    parser_new_request.add_argument('--email', dest='email', required=True,
                                    help='Requestor email address')
    parser_new_request.add_argument('--org', dest='org',
                                    help='organization unit (default: from CSR / empty)')
    parser_new_request.add_argument('--no-publish', dest='publish', action='store_false', default=True,
                                    help='Publish the certificate (default: publish)')
    parser_new_request.add_argument('--csrfile', dest='csr_file', required=True,
                                    help='csr file name')
    parser_new_request.add_argument('--pinfile', dest='pin_file', required=True,
                                    help='revocation PIN file name')
    parser_new_request.add_argument('--serfile', dest='serial_file', required=True,
                                    help='request serial file name')
    parser_new_request.set_defaults(func=new_request)

    parser_get_printout = subparsers.add_parser('getprintout', help='Get request printout by request serial')
    parser_get_printout.add_argument('--pinfile', dest='pin_file', required=True,
                                     help='revocation PIN file name')
    parser_get_printout.add_argument('--serfile', dest='serial_file', required=True,
                                     help='request serial file name')
    parser_get_printout.add_argument('--printoutfile', dest='printout_file', required=True,
                                     help='Printout file')
    parser_get_printout.set_defaults(func=get_printout)

    parser_get_certificate = subparsers.add_parser('getcert', help='Get certificate by request serial')
    parser_get_certificate.add_argument('--pinfile', dest='pin_file', required=True,
                                        help='revocation PIN file name')
    parser_get_certificate.add_argument('--serfile', dest='serial_file', required=True,
                                        help='request serial file name')
    parser_get_certificate.add_argument('--certfile', dest='cert_file', required=True,
                                        help='Certificate file')
    parser_get_certificate.set_defaults(func=get_certificate)

    args = parser.parse_args()
    # Calling without any argument must be handled explicitly
    if not hasattr(args, 'func'):
        parser.print_help()
        sys.exit(1)
    return args


def new_request(arguments):
    """Upload CSR to the CA"""
    if Path(arguments.serial_file).exists():
        print('Serial File already exists.')
        sys.exit(1)

    api = DFNPKI(arguments.ca)
    pin = get_pin_as_sha1(arguments.pin_file)
    csr, dns_san, ip_san, ou = load_csr(arguments.csr_file)

    if arguments.san is not None:
        san = arguments.san
    else:
        dns_san = arguments.dnssan if arguments.dnssan is not None else dns_san
        ip_san = arguments.ipsan if arguments.ipsan is not None else ip_san
        san = build_sanlist(dns_san, ip_san)

    ou = arguments.org if arguments.org is not None else ou

    try:
        response = api.submit_request(
            ra=arguments.ra_id,
            csr=csr,
            subj_alt_names=san,
            role=arguments.role,
            revocation_passphrase=pin,
            requester_name=arguments.name,
            requester_email=arguments.email,
            requester_ou=ou,
            publish=arguments.publish
        )
    except Error as f:
        print(f'DFN error message: {f.message}')
        sys.exit(1)
    write_file(arguments.serial_file, str(response).encode('utf-8'))


def get_printout(arguments):
    """Get a request pdf from the CA"""
    api = DFNPKI(arguments.ca)
    pin = get_pin_as_sha1(arguments.pin_file)
    serial = get_file(arguments.serial_file)
    try:
        response = api.retrieve_request_form(
            ra=arguments.ra_id,
            request_number=serial,
            revocation_passphrase=pin
        )
    except Error as f:
        print(f'DFN error message: {f.message}')
        sys.exit(1)
    write_file(arguments.printout_file, response, True)


def get_certificate(arguments):
    """Get a signed certificate by request serial from the CA"""
    api = DFNPKI(arguments.ca)
    pin = get_pin_as_sha1(arguments.pin_file)
    serial = get_file(arguments.serial_file)
    try:
        response = api.retrieve_certificate(
            ra=arguments.ra_id,
            request_number=serial,
            revocation_passphrase=pin
        )
    except Error as f:
        print(f'DFN error message: {f.message}')
        sys.exit(1)
    if response is None:
        print('No certificate ready for collection')
        sys.exit(4)
    else:
        write_file(arguments.cert_file, response, False)


def build_sanlist(dnssan, ipsan):
    """Build the SaN list for CA"""
    san = []
    san += [f'DNS:{item}' for item in dnssan]
    san += [f'IP:{ipaddress.ip_address(item).compressed}' for item in ipsan]
    return san


def get_pin_as_sha1(file_name):
    """Get pin from file"""
    pin = get_file(file_name).rstrip()
    return hashlib.sha1(pin.encode('utf-8')).hexdigest()


def load_csr(file_name: str) -> (str, List[str], List[str], str):
    """Get csr from file"""
    with open(file_name, 'rb') as f:
        csr_bytes = f.read()
    csr_obj = x509.load_pem_x509_csr(csr_bytes)

    csr = csr_obj.public_bytes(Encoding.PEM).decode('utf-8')
    try:
        subject_alternative_names = csr_obj.extensions.get_extension_for_class(x509.SubjectAlternativeName).value
        dns_san = [str(san.value) for san in subject_alternative_names if isinstance(san, x509.DNSName)]
        ip_san = [str(san.value) for san in subject_alternative_names if isinstance(san, x509.IPAddress)]
    except x509.extensions.ExtensionNotFound:
        dns_san = []
        ip_san = []

    cn = csr_obj.subject.get_attributes_for_oid(x509.OID_COMMON_NAME)[0]
    dns_san.append(cn.value)

    ou_list = csr_obj.subject.get_attributes_for_oid(x509.OID_ORGANIZATIONAL_UNIT_NAME)
    if len(ou_list) == 1:
        ou = ou_list[0].value
    elif len(ou_list) == 0:
        ou = ''
    else:
        print('Error: Found multiple OU in CSR')
        sys.exit(1)

    return csr, dns_san, ip_san, ou


def get_file(file_name):
    """Get file"""
    with open(file_name, 'r') as f:
        data = f.read()
    return data


def write_file(file_name, data, binary=True):
    """Write file"""
    if binary:
        mode = 'wb'
    else:
        mode = 'w'
    with os.fdopen(os.open(file_name, os.O_WRONLY | os.O_CREAT | os.O_EXCL, 0o600), mode) as f:
        f.write(data)


def main():
    args = get_args()
    if args.debug:
        import logging

        logging.basicConfig(level=logging.DEBUG)
    args.func(args)


if __name__ == '__main__':
    main()
