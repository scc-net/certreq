# certreq.py
This script automates the submission of KIT CA certificate requests.

## Info
This is a hard fork of [tls-request](https://git.scc.kit.edu/ttuellmann/tls-request)
